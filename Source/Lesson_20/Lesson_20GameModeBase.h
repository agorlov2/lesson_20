// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Lesson_20GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class LESSON_20_API ALesson_20GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
