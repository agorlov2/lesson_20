// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementsBase;
UENUM()
enum class EmovmentDiretion
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class LESSON_20_API ASnakeBase : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementsBase> SnakeElementsClass;

	UPROPERTY()
		TArray<ASnakeElementsBase*> SnakeElements;

	UPROPERTY(EditDefaultsOnly)
		float ElemtSize;
	
	UPROPERTY()
		EmovmentDiretion LastMoveDirection;
	
	UPROPERTY(EditDefaultsOnly)
		float MoveSpeed;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddSnakeElement(int ElementsNum = 1);

	void Move();

	UFUNCTION()
		void SnakeElementOverlap(ASnakeElementsBase* OverlappedElement,AActor* Other);
};
