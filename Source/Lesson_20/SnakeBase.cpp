// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementsBase.h"
#include "Interactable.h"



// Sets default values
ASnakeBase::ASnakeBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElemtSize = 100.f;
	LastMoveDirection = EmovmentDiretion::DOWN;
	MoveSpeed = 0.f;


}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	//GetWorld()->SpawnActor<ASnakeElementsBase>(SnakeElementsClass, GetActorTransform());
	AddSnakeElement(4);

}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetActorTickInterval(MoveSpeed);
	Move();

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{

		FVector NewLocation(SnakeElements.Num() * ElemtSize, 0, 0);
		FTransform NewTransform(NewLocation);

		//FTransform NewtTransform = FTransform(GetActorLocation() - FVector(SnakeElements.Num() * ElemtSize, 0, 0));

		ASnakeElementsBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementsBase>(SnakeElementsClass, NewTransform);
		//NewSnakeElem->SetActorHiddenInGame(false);
		//NewSnakeElem->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
			
		}

	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	/*float Movementspeed = ElemtSize;*/

	switch (LastMoveDirection)
	{
	case EmovmentDiretion::UP:
		MovementVector.X += ElemtSize;
		break;

	case EmovmentDiretion::DOWN:
		MovementVector.X -= ElemtSize;
		break;

	case EmovmentDiretion::LEFT:
		MovementVector.Y += ElemtSize;
		break;

	case EmovmentDiretion::RIGHT:
		MovementVector.Y -= ElemtSize;
		break;




	}

	//AddActorLocalOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	SnakeElements[0]->AddActorLocalOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementsBase* OverlappedElement, AActor* Other)
{

	if(IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if(InteractableInterface)
		{
			InteractableInterface->Interact(this,bIsFirst);
		}
	}
	
}




